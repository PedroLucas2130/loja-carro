﻿using LojaCarro;
using System;

namespace Teste
{
    class Program
    {
        static void Main(string[] args)
        {
            Carro carro = new Carro();
            carro.Marca = "Renault";
            carro.Modelo = "Sandero";
            carro.Preco = 49000;
            carro.Km = 0;
            carro.Ano = 2017;
            carro.NumeroDonos = 1;

            double valorparcela = carro.ValorParcela(20);

            string tipoCarro = carro.TipoCarro();

            Console.WriteLine(tipoCarro);
            
            //carro.TipoCarro();

            //Console.WriteLine(carro.Marca);
        }
    }
}
